import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { QuestionService } from '../../services/question.service';
import { Question } from 'src/app/models/question';
import { from } from 'rxjs';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
    selector: 'app-question-list',
    templateUrl: './question-list.component.html',
    styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnInit, AfterViewInit {

    public displayedColumns = ['title', 'answer_count', 'creation_date', 'owner'];

    public dataSource = new MatTableDataSource<Question>();

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private questionService: QuestionService) {
    }

    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }

    ngOnInit() {
    }

    public search = (value: string) => {
        this.questionService.get(value.trim().toLocaleLowerCase()).subscribe(data => {
            this.dataSource.data = data;
        });
    }

    //TODO нормальный paging с сервером
    //TODO фильтр 'только отвеченные' 

}
