import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatTabsModule, MatSidenavModule, MatToolbarModule, MatIconModule, MatButtonModule, MatListModule, MatMenuModule,
    MatTableModule, MatSortModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatChipsModule
} from '@angular/material';

@NgModule({
    imports: [
        MatPaginatorModule,
        MatFormFieldModule,
        MatInputModule,
        MatSortModule,
        MatTableModule,
        MatMenuModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        CommonModule,
        MatTabsModule,
        MatSidenavModule,
        MatChipsModule
    ],
    exports: [
        MatPaginatorModule,
        MatFormFieldModule,
        MatInputModule,
        MatSortModule,
        MatTableModule,
        MatMenuModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        MatTabsModule,
        MatSidenavModule,
        MatChipsModule
    ],
    declarations: []
})
export class MaterialModule { }