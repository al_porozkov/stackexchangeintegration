import { Owner } from './owner';

export class Question {
    title: string;
    is_answered: boolean;
    creation_date: Date;
    question_id: number;
    answer_count: number;
    owner: Owner;
    link: String;
}
