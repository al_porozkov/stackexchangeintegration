import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Question } from '../models/question';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';

@Injectable()
export class QuestionService {

    constructor(private http: HttpClient) {
    }

    public get(intitle: string): Observable<Question[]> {
        return this.http.get<Question[]>(`${environment.urlAddress}/question?intitle=${intitle}`);
    }
}
