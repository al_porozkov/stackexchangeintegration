package stackexchange.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import stackexchange.model.Question;
import stackexchange.service.StackExchangeService;

@RestController
@RequestMapping("/question")
@CrossOrigin(origins = "http://localhost:4200")
public class QuestionController {

    private StackExchangeService stackExchangeService;

    @Autowired
    public QuestionController(StackExchangeService stackExchangeService) {
        this.stackExchangeService = stackExchangeService;
    }

    @GetMapping
    public Question[] getQuestions(@RequestParam(value = "intitle") String intitle,
                                   @RequestParam(value = "pageSize") @Nullable Long pageSize,
                                   @RequestParam(value = "page") @Nullable Long page) {
        return stackExchangeService.getQuestions(intitle, pageSize, page);
    }

}