package stackexchange.model;

import java.sql.Timestamp;

public class Question {

    private String title;

    private boolean is_answered;

    private Timestamp creation_date;

    private String link;

    private Owner owner;

    private long question_id;

    private long answer_count;

    public String getTitle() {
        return title;
    }

    public boolean isIs_answered() {
        return is_answered;
    }

    public Timestamp getCreation_date() {
        return creation_date;
    }

    public String getLink() {
        return link;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setIs_answered(boolean is_answered) {
        this.is_answered = is_answered;
    }

    public void setCreation_date(Timestamp creation_date) {
        this.creation_date = creation_date;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }


    public long getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(long question_id) {
        this.question_id = question_id;
    }

    public long getAnswer_count() {
        return answer_count;
    }

    public void setAnswer_count(long answer_count) {
        this.answer_count = answer_count;
    }
}
