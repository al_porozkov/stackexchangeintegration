package stackexchange.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import stackexchange.model.Question;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public abstract class AbstractStackExchangeService implements StackExchangeService {

    private String query = "http://api.stackexchange.com/2.2/search?order=desc&sort=activity&site=%s&intitle=\"%s\"&pagesize=%d&page=%d";

    private HttpClient httpClient;

    protected abstract String getSite();

    @Autowired
    public AbstractStackExchangeService(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public Question[] getQuestions(String intitle, @Nullable Long pageSize, @Nullable Long page) {
        pageSize = pageSize != null ? pageSize : 100;
        page = page != null ? page : 1;

        try {
            String url = String.format(query, getSite(), intitle, pageSize, page);
            String response = httpClient.getRequest(url);

            return serialize(response);
        } catch (Throwable cause) {
            return new Question[0];
        }
    }

    private Question[] serialize(String json) throws IOException {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Timestamp.class, new TimestampAdapter())
                .create();

        ObjectMapper mapper = new ObjectMapper();

        JsonNode rootNode = mapper.readTree(json);
        String items = rootNode.path("items").toString();

        return gson.fromJson(items, Question[].class);
    }

    public class TimestampAdapter extends TypeAdapter<Timestamp> {

        @Override
        public Timestamp read(JsonReader in) throws IOException {
            return new Timestamp(in.nextLong() * 1000);  // convert seconds to milliseconds
        }

        @Override
        public void write(JsonWriter out, Timestamp timestamp) throws IOException {
            out.value(timestamp.getTime() / 1000);  // convert milliseconds to seconds
        }
    }
}
