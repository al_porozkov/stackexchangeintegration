package stackexchange.service;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.zip.GZIPInputStream;

@Service
public class HttpClient {

    private String charset = "utf-8";

    public String getRequest(String url) {
        try {
            URL reqUrl = new URL(url);
            URL requestUrl = new URI(reqUrl.getProtocol(), reqUrl.getUserInfo(), reqUrl.getHost(), reqUrl.getPort(), reqUrl.getPath(), reqUrl.getQuery(), reqUrl.getRef()).toURL();
            HttpURLConnection connection = (HttpURLConnection) requestUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStreamReader inputStreamReader = null;
                if ("gzip".equals(connection.getContentEncoding())) {
                    inputStreamReader = new InputStreamReader(new GZIPInputStream(connection.getInputStream()), charset);
                } else {
                    inputStreamReader = new InputStreamReader(connection.getInputStream(), charset);
                }

                BufferedReader reader = new BufferedReader(inputStreamReader);
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = reader.readLine()) != null) {
                    response.append(inputLine);
                }
                reader.close();

                return response.toString();
            } else {
                return connection.getResponseCode() + connection.getResponseMessage();
            }
        } catch (Throwable cause) {
            return cause.getMessage();
        }
    }

}
