package stackexchange.service;

import stackexchange.model.Question;

public interface StackExchangeService {

    public Question[] getQuestions(String intitle, Long pageSize, Long page);

}
