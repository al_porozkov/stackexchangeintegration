package stackexchange.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stackexchange.model.Question;
import java.io.*;

@Service
public class StackOverflowService extends AbstractStackExchangeService {

    private String site = "stackoverflow";

    @Override
    protected String getSite() {
        return this.site;
    }

    public StackOverflowService(HttpClient httpClient) {
        super(httpClient);
    }


}
